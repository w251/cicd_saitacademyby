## Project's reporter: Eduard Linkevich
## Group number: md-sa2-18-21

## Description of application for deployment
+ Name of application:
[FluxBB](https://fluxbb.org/downloads/)


+ FluxBB is written in PHP programming language. 
FluxBB is an open-source forum application released under the GNU General Public Licence. It is free to download and use and will remain so. FluxBB was conceived and designed to be fast and light with less of the "not so essential" features that some of the other forums have whilst not sacrificing essential functionality or usability.

+ What kind of DB: MYSQL

+ Link on git repository/site/package repository
[cicd_project](https://gitlab.com/w251/cicd_saitacademyby)
[ws_project](https://gitlab.com/w251/fluxbb_saitacademyby)

## Pipeline. High Level Design

![scheme](scheme.png)

## Technologies which were used in project
Orchestration: GitLab CI/CD

Automation tools: Bash, Docker

CI description: by hook/schedule/poll, tests, checks ....

## Deployment flows short description:

### step 1
developer makes commit/merge, after that gets slack notification. 
developer starts assembly ws service creats tag into ws_project, this triggers pipeline into cicd_project

### step 2
the orchestrator receives the project code, ckecks validation the config files.
on this step into artifactory uploads release version for saving, if step fails with an error, from artifactory downloads previos release
and installed on environment.

### step 3
start stop ws services, upgrade application, starting and checking the running application.


## How the pipeline for installing or rollback is launched
tag to install
```
R001|R000|main

R001 - release version
R000 - previos release
main - branch, standard installation
```

tag to rollback
```
R000||rollback
R000 - release for rturn
rollback - - branch, rollback installation
```

we can also run the pipeline using a trigger without a developer
```bash
      curl -X POST \
           -F token=$TOKEN \
           -F ref=$REF \
           -F "variables[NEW_TAG]=$NEW_TAG" \
           -F "variables[PREV_TAG]=$PREV_TAG" \
           https://gitlab.com/api/v4/projects/31053357/trigger/pipeline

```



## Links
[jfrog](https://jfrog.com/artifactory/install/)
