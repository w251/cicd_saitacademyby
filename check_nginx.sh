#!/bin/bash
#1-SCPPWD
#2-SCPUSER
#3-SCPIP
cnt=0


while [ $cnt -lt 5 ]
  do
  chk_nginx=$(sshpass -p "$1" ssh "$2"@"$3" "sudo service nginx status" | grep 'Stopped A high performance web server and a reverse proxy server' | wc -l)
  cnt=$(( $cnt + 1 )) && echo $cnt $chk_nginx
  if [ $chk_nginx -eq 0 ]
    then 
	  echo "nginx unstoped"
	  sleep 10
  elif [ $chk_nginx -eq 1 ]
    then 
	  echo "nginx stoped"
	  exit 0
  elif [ $cnt -eq 5 ]
    then 
	  echo "ERROR, nginx unstoped"
	  exit 1
  fi
done