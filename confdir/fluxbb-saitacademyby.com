server {
        listen 9010;
        listen [::]:9010;

        server_name fluxbb-saitacademyby.com;

        root /opt/fluxbb_saitacademyby;
        index index.php;

        location / {
                try_files $uri $uri/ =404;
        }

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php7.4-fpm.sock;
                #fastcgi_index (index.html|index.php|index.htm);
                 
        }

}

