## OtherLinks
[mysql](https://losst.ru/ustanovka-mysql-ubuntu-16-04)
[fluxbb](https://fluxbb.org/downloads/)
[nginx](https://losst.ru/ustanovka-nginx-ubuntu-16-04)
[php+nginx](https://develike.com/ru/stati/nastrojka-nginx-dlya-podderzhki-php-na-debian-ubuntu)
[php+nginx2](https://habr.com/ru/post/320036/)
[cicd_project](https://gitlab.com/w251/cicd_saitacademyby)
[ws_project](https://gitlab.com/w251/fluxbb_saitacademyby)


### nginx
```bash
sudo apt install nginx
sudo systemctl enable nginx
sudo -u eduard nginx -t
sudo nginx -t
sudo service nginx reload
sudo service nginx restart
```