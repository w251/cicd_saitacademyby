server {
        listen 9090;
        listen [::]:9090;

        server_name work-saitacademyby.com;

        root /home/eduard/Documents/fluxbb_saitacademyby;
        index index.php;

        location / {
                try_files $uri $uri/ =404;
        }

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php7.4-fpm.sock;
                #fastcgi_index (index.html|index.php|index.htm);
                 
        }

}

